"       _
"__   _(_)_ __ ___  _ __ ___
"\ \ / / | '_ ` _ \| '__/ __|
" \ V /| | | | | | | | | (__
"  \_/ |_|_| |_| |_|_|  \___|

"===========PLUGINS=============
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'neoclide/coc.nvim', {'branch': 'release'}
"Plugin 'vim-syntastic/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'jiangmiao/auto-pairs'
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'
Plugin 'ryanoasis/vim-devicons'
Plugin 'ctrlpvim/ctrlp.vim' " fuzzy find files
Plugin 'scrooloose/nerdcommenter'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'arcticicestudio/nord-vim'
Plugin 'HerringtonDarkholme/yats.vim' " TS Syntax
call vundle#end()            " required
filetype plugin indent on    " required
"===============================

"  Display the line numbers
set number
set relativenumber

" Display the syntax colors of the language
syntax on

" Set the encoding to unicode
set encoding=utf-8

" Autocomplete for the vim menu
set wildmenu
set wildmode=list:longest,full

" Do not autocomment new lines
autocmd Filetype * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Hide tmux bar when entering vim
autocmd VimEnter,VimLeave * silent !tmux set status

" Make splits appear on the bottom and right
set splitbelow splitright

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" Spell-check set to F1, for Spanish:
"map <F1> :setlocal spell! spelllang=es_es<CR>

" Spell-check set to F2 for English:
"map <F2> :setlocal spell! spelllang=en_us<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
	map <leader>oe :setlocal spell! spelllang=en_us<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
	map <leader>os :setlocal spell! spelllang=es_es<CR>

" Map F3 to indenting
map <F3> gg=G

" Get rid of trailing whitespaces
autocmd BufWritePre * %s/\s\+$//e

" Operate with visual lines instead of logical ones
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" Shortcuts for split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Use Ctrl and arrows to move between tabs
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>

" Open NERDTree with Ctrl + n
map <C-n> :NERDTreeToggle<CR>
" If the only split left is NERDTree, close vim
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle

" Powerline configuration
"let g:powerline_pycmd="py3"
":set laststatus=2

" Air-line configuration
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline_symbols.linenr = 'ln'
let g:airline_symbols.maxlinenr = ' Ξ'
let g:airline_theme='angr'
let g:airline_symbols.whitespace = '_'

" Uncomment this line to disable YouCompleteMe plug-in
"let g:loaded_youcompleteme = 1

" Disbale the anoying beep
set noeb vb t_vb=

" Copy and paste from the clipboard
set clipboard+=unnamedplus

" Go back to normal mode with jk or kj
inoremap kj <C-C>
inoremap jk <C-C>

" Set the colorscheme to be able to use the spellcheck properly
:colorscheme nord

" Compile document, be it groff/LaTeX/markdown/etc.
	map <F4> :w! \| !compiler <c-r>%<CR>
"======Completitions======

"HTML
" Create the skeleton of a web page
autocmd Filetype html nnoremap <F5> i<!DOCTYPE html><Enter><html lang="es"><Enter><head><Enter><meta CHARSET="UTF-8"><Enter><title>Title</title><Enter></head><Enter><body><Enter><main><Enter></main><Enter></body><Enter></html><Esc>3ko

autocmd FileType html inoremap ,1 <h1></h1><Esc>4hi
autocmd FileType html inoremap ,2 <h2></h2><Esc>4hi
autocmd FileType html inoremap ,3 <h3></h3><Esc>4hi
autocmd FileType html inoremap ,p <p></p><Esc>3hi
autocmd FileType html inoremap ,b <b></b><Esc>3hi
autocmd FileType html inoremap ,it <i></i><Esc>3hi
autocmd Filetype html inoremap ,a <a href=""></a><Esc>3hi
autocmd FileType html inoremap ,ul <ul><Enter><li></li><Enter></ul><Esc>01kf<4li
autocmd FileType html inoremap ,ol <ol><Enter><li></li><Enter></ol><Esc>01kf<4li
autocmd FileType html inoremap ,li <li></li><Esc>4hi
autocmd FileType html inoremap ,im <img src="" alt=""><Esc>hi
autocmd Filetype html inoremap ,c <!--<space><space><space>--><Esc>4hi
autocmd Filetype html inoremap ,sec <section><Enter></section><Esc>ko
autocmd Filetype html inoremap ,div <div><Enter></div><Esc>ko
autocmd Filetype html inoremap ,vi <video src="" controls></video><Esc>4bla
autocmd Filetype html inoremap ,scr <script src=""></script><Esc>2bla

"MARKDOWN
"create the header of a Generic document
autocmd Filetype rmd nnoremap <F5> i---<Enter>title:<space>""<Enter>author:<space>"Ángel<space>García<space>Menéndez"<Enter>email:<space>"UO258654@uniovi.es"<Enter>output:<Enter><Tab>pdf_document:<Enter><Tab>latex_engine:<space>lualatex<Enter>toc:<space>true<Enter><Esc>0i---<Enter><Enter><Esc>ggj$i

autocmd Filetype markdown,rmd inoremap ,r ```r<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,p ```python<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,b ```bash<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,j ```java<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,c ```c++<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,i ![]()<Esc>F[a
autocmd Filetype markdown,rmd inoremap ,a []()<Esc>F[a
autocmd Filetype markdown,rmd inoremap ,h ```html<Enter>```<Enter><Enter><esc>3ko
autocmd Filetype markdown,rmd inoremap ,x ```xml<CR>```<CR><CR><esc>2kO

"GROFF
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd Filetype nroff inoremap .IP .IP<space>\(bu<space>2

" TEX
autocmd BufRead,BufNewFile *.tex set filetype=tex
" Runs a script that cleans out tex build files whenever I close out of a .tex file.
autocmd VimLeave *.tex !texclear %
autocmd Filetype tex nnoremap <F5> i\documentclass{article}<Enter>\usepackage[T1]{fontenc}<Enter>\usepackage{lmodern}<Enter>\usepackage{parskip}<Enter>\usepackage{hyperref}<Enter>\usepackage{natbib}<Enter>\usepackage{graphicx}<Enter>\usepackage[utf8]{inputenc}<Enter>\usepackage[spanish]{babel}<Enter>\usepackage{csquotes}<Enter><Enter>\title{}<Enter>\author{Ángel García Menéndez}<Enter>\date{}<Enter><Enter>\begin{document}<Enter>\maketitle<Enter>%\tableofcontents<Enter>%\newpage<Enter><Enter>\section{}<Enter><Enter>%\bibliographystyle{abbrv}<Enter>%\bibliography{main}<Enter><Enter>\end{document}<esc>13ki

autocmd FileType tex inoremap ,-1 \part{}<Esc>i
autocmd FileType tex inoremap ,0 \chapter{}<Esc>i
autocmd FileType tex inoremap ,1 \section{}<Esc>i
autocmd FileType tex inoremap ,2 \subsection{}<Esc>i
autocmd FileType tex inoremap ,3 \subsubsection{}<Esc>i
autocmd FileType tex inoremap ,b \textbf{}<Esc>i
autocmd FileType tex inoremap ,i \textit{}<Esc>i
autocmd Filetype tex inoremap ,a \href{}{}<Esc>i
autocmd FileType tex inoremap ,ol \begin{enumerate}<Enter>\item<Enter>\end{enumerate}<Esc>kA<space>
autocmd FileType tex inoremap ,ul \begin{itemize}<Enter>\item<Enter>\end{itemize}<Esc>kA<space>

" coc config
let g:coc_global_extensions = [
  \ 'coc-snippets',
  \ 'coc-pairs',
  \ 'coc-tsserver',
  \ 'coc-eslint',
  \ 'coc-prettier',
  \ 'coc-json',
  \ ]
" from readme
" if hidden is not set, TextEdit might fail.
set hidden " Some servers have issues with backup files, see #649 set nobackup set nowritebackup " Better display for messages set cmdheight=2 " You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <F2> <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <C-d> <Plug>(coc-range-select)
xmap <silent> <C-d> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
