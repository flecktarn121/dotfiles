# _________  _   _ ____   ____
#|__  / ___|| | | |  _ \ / ___|
#  / /\___ \| |_| | |_) | |
# / /_ ___) |  _  |  _ <| |___
#/____|____/|_| |_|_| \_\\____|

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Enable colors
autoload -U colors && colors

setopt prompt_subst
PS1=" %B%{$fg[green]%}%n %{$fg[blue]%}%c%{$reset_color%}%b  "
#PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg_no_bold[yellow]%}%1~ %{$reset_color%}%#"
##RPROMPT="[%{$fg_no_bold[yellow]%}%?%{$reset_color%}]"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v

# Enter insert mode with kj or jk instead of ESC
export KEYTIMEOUT=4
bindkey -M viins 'jk' vi-cmd-mode
bindkey -M viins 'kj' vi-cmd-mode

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Make the delete key, you know, delete
bindkey "\e[3~" delete-char

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
#(cat ~/.cache/wal/sequences &)

# If not running interactively, do not do anything
[[ $- != *i* ]] && return
[[ -z "$TMUX" ]] && exec tmux -f ~/.config/tmux/.tmux.conf

# Change cursor shape for different vi modes (neovim style).
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

#=====ALIASES=======

# Mount a device with read-write permissions for all users
# Instead of this, use udisks2 with udiskies
alias mount-rw="sudo mount -o gid=users,fmask=113,dmask=002"

# Download from youtube with avaliable subs embedded
alias yd="youtube-dl -i --add-metadata --all-subs --embed-subs -f best -o '%(title)s.%(ext)s'"

# Download just audio from youtube
alias yda="youtube-dl -x -f bestaudio/best --audio-format vorbis -o '%(title)s.%(ext)s'"

# Call neovim instead of vim (why does this feels wrong?)
alias v="nvim"

alias uu="udiskie-umount"

function cpr() {
  rsync --archive -hh --partial --info=stats1 --info=progress2 --modify-window=1 "$@"
}

function mvr() {
  rsync --archive -hh --partial --info=stats1 --info=progress2 --modify-window=1 --remove-source-files "$@"
}
#====================

# Turn off the annoying beep
# Who thought it was a good idea?
unsetopt BEEP

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
