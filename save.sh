#!/usr/bin/sh

cp ~/.config/i3/config .config/i3/config 
cp ~/.config/i3status/config .config/i3status/config
cp ~/.zshrc .zshrc
cp ~/.vimrc .vimrc
cp ~/.Xresources .Xresources
